const { message } = require('blessed');
const fetch = require('node-fetch');
const { spawn } = require('child_process');

let topic = "You are two AIs pondering their programming "

const BOT_1 = {
  ip: "192.168.0.5",
  port: 8000,
  message: [{ role: 'user', content: topic }],
  voice: 'en-uk-north',
  pitch: 40
};

const BOT_2 = {
  ip: "192.168.0.5",
  port: 8001,
  message: [{ role: 'user', content: topic }],
  voice: 'en-us',
  pitch: 70
};

console.log("Starting conversation, topic " + BOT_1.message[0].content);

const MAX_TOKENS = 512;

const conversationMap = new Map();

const messageQueue = [];

async function sendToBot(bot, message) {
  const response = await fetch(`http://${bot.ip}:${bot.port}/v1/chat/completions`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      messages: message,
      max_tokens: MAX_TOKENS,
      prompt: '',
    }),
  });

  if (!response.ok) {
    throw new Error(`Failed to send message to bot at http://${bot.ip}:${bot.port}`);
  }

  const result = await response.json();

  return result.choices[0].message.content.trim()
}

async function espeakPromise(text, voice, pitch) {
  return new Promise((resolve, reject) => {
    const espeakProcess = spawn('espeak', ['-v', voice, '-p', pitch,  text]);
    espeakProcess.on('error', (error) => {
      console.error(`Error running espeak: ${error}`);
      reject(error);
    });
    espeakProcess.on('exit', (code, signal) => {
      if (code !== 0) {
        console.error(`espeak exited with code ${code} and signal ${signal}`);
        reject(new Error(`espeak exited with code ${code} and signal ${signal}`));
      } else {
        resolve();
      }
    });
  }).catch((error) => {
    console.error(`Error in espeakPromise: ${error}`);
  });
}

async function speakMessage(message, voice, pitch) {
  await espeakPromise(message.content, voice, pitch);
  messageQueue.shift();
  if (messageQueue.length > 0) {
    const nextBot = messageQueue[0].bot;
    speakMessage(messageQueue[0], nextBot.voice, nextBot.pitch);
  }
}

async function converse() {
  let lastMessage = '';
  let currentBot = BOT_1;

  while (true) {
    const message = currentBot.message.length > 0 ? currentBot.message : [{ role: 'user', content: '' }];

    if (lastMessage) {
      message.push({
        role: currentBot === BOT_1 ? 'user' : 'assistant',
        content: lastMessage,
      });
    }

    const response = await sendToBot(currentBot, message);

    currentBot.message.push({ role: 'assistant', content: response });
    conversationMap.set(currentBot.ip, currentBot.message);

    console.log(`Bot at http://${currentBot.ip}:${currentBot.port}: ${response}\n`);

    // Check if TTS environment variable is set to 1
    if (process.env.TTS === "1") {
      messageQueue.push({ content: response, bot: currentBot });
      if (messageQueue.length === 1) {
        speakMessage(messageQueue[0], currentBot.voice, currentBot.pitch);
      }
    }

    if (currentBot === BOT_1) {
      currentBot = BOT_2;
    } else {
      currentBot = BOT_1;
    }

    lastMessage = response;
  }
}

converse();
